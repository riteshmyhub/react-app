function header() {
    let title = document.getElementsByTagName("header")[0].getAttribute('data-heading');
    document.getElementById('bootstrap-init').innerHTML = `
    <div style="width: 300px;" class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasExampleLabel">React Docs</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <ul class="nav flex-column" id="docs-list">
    
            <!-- dynamic-docs-list -->
            <!-- dynamic-docs-list -->
    
            </ul>
        </div>
    </div>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand t-t-c" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">
        <i class="bi bi-list"></i>&nbsp;&nbsp;${title ? title : 'Home'}
        </a>
        <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="/react-app">Home</a>
            <a class="nav-link" href="#">Features</a>
            <a class="nav-link" href="#">Pricing</a>
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          </div>
      </div>
    </nav>`
}


function docsList() {
    const list = [
        'introduction',
        'set-up',
        'app-booting',
        'fragment',
        'template-literals-in-JSX',
        'JSX-attribute-rules',
        'internal-css'
    ]

    let ele = document.getElementById("docs-list")

    list.forEach(element => {
        return ele.innerHTML += `
          <li class="nav-item">
            <a class="nav-link fs-6 t-t-c" aria-current="page"
              href="/react-app/docs/${element}.html">${element}</a>
         </li>`
    });
}
header();
docsList();


function main() {
    document.getElementById('main').innerHTML = `
    <div class="container-fluid">
        <div class="row">
          <div class="col-9">
              ${document.getElementById("data-content").innerHTML}
          </div>
          <div class="col center-xy h-100 bg-light" id="side-list">
          <ul class="nav flex-column w-100">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Active</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          </li>
        </ul>
          </div>
     </div>
</div>`
}

main();