function createScript() {
    var scriptTag = document.createElement("script");
    //scriptTag.setAttribute("type", "text/javascript");
    scriptTag.setAttribute("src", "/react-app/public/Js/prism.js");
    document.getElementsByTagName("head")[0].appendChild(scriptTag);
}


function injectAppCss() {
    var AppCss = document.createElement("link");
    AppCss.setAttribute("rel", "stylesheet");
    AppCss.setAttribute("href", "/react-app/style.css");
    document.getElementsByTagName("head")[0].appendChild(AppCss);
}
function injectBootstrap() {
    var bootstrap = document.createElement("link");
    const bs5 = 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css'
    bootstrap.setAttribute("rel", "stylesheet");
    bootstrap.setAttribute("href", bs5);
    document.getElementsByTagName("head")[0].appendChild(bootstrap);
}
injectBootstrap()

injectAppCss()

createScript()


function popperJs() {
    let popper = document.createElement("script");
    //scriptTag.setAttribute("type", "text/javascript");
    popper.setAttribute("src", "https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js");
    document.getElementsByTagName("body")[0].appendChild(popper);
}

function bootstrapJS() {
    var bootJS = document.createElement("script");
    //scriptTag.setAttribute("type", "text/javascript");
    bootJS.setAttribute("src", "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js");
    document.getElementsByTagName("body")[0].appendChild(bootJS);
}

function componentsJs() {
    let components = document.createElement("script");
    //scriptTag.setAttribute("type", "text/javascript");
    components.setAttribute("src", "/react-app/public/components/components.js");
    document.getElementsByTagName("body")[0].appendChild(components);
}

popperJs();
bootstrapJS();
componentsJs();